# Boappa Interview Project #

A simple Node.js and React app using MongoDB with a RESTful API to list, create and remove members. Axios was used to fetch data from the API. Basic styling was provided by the Semantic UI Library. 


## Setup ##
__Step 1:__ Clone the repo.  
__Step 2:__  Get server running. In terminal, type the follow commands:  
in terminal...
```
cd boappa
nodemon server.js
```
__Step 3:__ Get client running. In terminal, type the following commands:  
in terminal... 
```
cd boappa/client
npm start
```


## Remarks ##
#### Challenges
The most challenging part of the project was getting the axios requests work. I had followed everything correctly on the documentation, but this became difficult because I kept encountering a `No 'Acess-Control-Allow-Origin' header is present on the failed requested resource.` error message. After researching this error I found it to be a CORS error. There were many suggestions on how to resolve it, many did not work. Now I know the simplest way is to install the `CORS` package to cover all my bases.  

While doing this project, I've learned that `.remove()` is depreciated in MongoDB. Another alternative would be to use `.deleteOne()`  

When trying to have the component auto reload the data every time a member was added or deleted, I could of added logic to remove or add to the memberList object that was already in state without actually making that extra call to the database. I am using a bit extra of resources here to achieve something that is always synced with the database and "real time". I could've went another route and used socket.io in the backend instead of having it dealt in the front end. But since this is a very small scale project, it is acceptable but for larger scale projects socket.io definitely will be needed.

Another thing worth mentioning was the importance of planning your course of action. I think it is so easy to get excited about a project, dive right in and start to write some code. I always find that it is best to write everything out, make a checklist, plan out what you're going to do, how you are going to do it and what technologies you will use for it. It can be easy to get overwhelmed by what you need to get done if I don't write a plan out.  

#### Future Improvements
- a prettier UI. i would try material-ui library for fun. 
- more validations
- make tests
- refactoring 
     * for speed
     * general code clean up
     * css (e.g. so i don't need to use !important)
     * folder structure/ naming conventions
- more efficient way to reload data instead of chaining AJAX calls. I would definitely use socket.io
- use sass instead of plain css  
- sorted/filtered/searchable table
- mongodb's ID is an object id that has a hash value. other db's may just start from 0 and increment up. previewing the id on the table with a large hash may not be very pretty. i would do an autoincrement field and use that as the "id". the requirements listed for "List current members (name, email, id)", i interpreted that name, email and id as what was needed for the members schema, while displaying the member would only show name and email because normally we would not show the ID. If you'd like to show the ID on the table, please let me know! I'll put it up.
- i definitely don't know everything so there can always be room to improve in all aspects
- I would love feedback for how I can improve! 

Send PayPal to: nhswong@gmail.com


