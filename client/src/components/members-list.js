import React, { Component } from 'react';
import { Table, Button, Message } from 'semantic-ui-react';
import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';


class MembersList extends Component {
    static propTypes = {
        membersList: PropTypes.array,
        deleteMember: PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.renderMembers = this.renderMembers.bind(this);
        this.renderEmpty = this.renderEmpty.bind(this);
        this.renderMembersLogic = this.renderMembersLogic.bind(this);
    }

    renderMembers() {
        const deleteMember = this.props.deleteMember;
        return (
            <Table striped padded>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Email</Table.HeaderCell>
                        <Table.HeaderCell>&nbsp;</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.props.membersList.map(member =>
                        <Table.Row key={member._id}>
                            <Table.Cell>{member.name}</Table.Cell>
                            <Table.Cell>{member.email}</Table.Cell>
                            <Table.Cell>
                                <Button icon='remove user' onClick={() => { deleteMember(member._id) }}/>
                            </Table.Cell>
                        </Table.Row>
                    )}
                </Table.Body>
            </Table>
        )
    }

    renderEmpty() {
        return (
            <div>
                <Message size="large" negative>
                    <Message.Header>Uh oh, seems like you have no current members...</Message.Header>
                    <p>Fill out the form above to add a new member!</p>
                </Message>
            </div>
        )
    }

    renderMembersLogic() {
        if (isEmpty(this.props.membersList) === true) {
            return (this.renderEmpty())
        } else {
            return (this.renderMembers())
        }
    }

    render() {
        return (
            <div className="members-list">
                <h3>Members List</h3>
                {this.renderMembersLogic()}
            </div>
        )
    }
}

export default MembersList;