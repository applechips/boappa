import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon } from 'semantic-ui-react';

class MemberForm extends Component {
    static propTypes = {
        addMember: PropTypes.func,
    }
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: ''
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        const state = this.state;
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit(e) {
        e.preventDefault();
        const {name, email} = this.state;
        const clear = {
            name: '',
            email: '',
        }
        this.props.addMember(name, email);
        this.setState(clear);
    }

    render() {

        return (
            <div className="member-form">
                <Form>
                    <Form.Group widths='equal'>
                        <Form.Input name='name' value={this.state.name} label='Name' placeholder='Name' onChange={this.onChange}/>
                        <Form.Input name='email' value={this.state.email} label='Email' placeholder='Email' onChange={this.onChange}/>
                    </Form.Group>
                    <Button color='olive' animated='fade' onClick={this.onSubmit}>
                        <Button.Content visible>Add a Member</Button.Content>
                        <Button.Content hidden>
                            <Icon name='add user'/>
                        </Button.Content>
                    </Button>

                </Form>
            </div>
        )
    }
}

export default MemberForm;