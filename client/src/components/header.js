import React, { Component } from 'react';
import { Image } from 'semantic-ui-react';
import logo from '../imgs/boappa-logo.svg';

class Header extends Component {
    render() {
        return (
            <div className="header">
                <Image className="logo" src={logo} size="small"/>
                <h1 className="title">Housing Society Members</h1>
            </div>
        )
    }
}

export default Header;