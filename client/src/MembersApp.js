import React, { Component } from 'react';
import axios from 'axios';
import Header from './components/header';
import Form from './components/member-form';
import MembersList from './components/members-list';
import './styles/MembersApp.css';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            members: this.fetchMembers()
        };

        this.fetchMembers = this.fetchMembers.bind(this)
        this.deleteMember = this.deleteMember.bind(this);
        this.addMember = this.addMember.bind(this);
    }


    fetchMembers() {
        axios.get('http://localhost:5000/api/members')
            .then(res => {
                this.setState({members: res.data});
            });
    }

    deleteMember(memberId) {
        axios.delete(`http://localhost:5000/api/members/${memberId}`)
            .then(res => {
                this.fetchMembers();
            })
            .catch(function (error) {
                console.log(error);
            });
        this.fetchMembers();
    }

    addMember(name, email) {
        axios.post('http://localhost:5000/api/members', {name: name, email: email})
            .then((res) => {
                this.fetchMembers();
            })
            .catch(function (error) {
                console.log(error);
            });
        this.fetchMembers();
    }

    render() {
        const members = this.state.members;
        return (
            <div className="root">
                <div className="container">
                    <Header />
                    <Form addMember={this.addMember}/>
                    <MembersList membersList={members} deleteMember={this.deleteMember}/>
                </div>
            </div>
        );
    }
}

export default App;
