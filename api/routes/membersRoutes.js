'use strict';

module.exports = function(app) {
    var members = require('../controllers/membersController');

    app.route('/api/members')
        .get(members.list_all_members)
        .post(members.create_a_member)


    app.route('/api/members/:memberId')
        .delete(members.delete_a_member);
};
