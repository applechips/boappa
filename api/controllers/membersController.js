'use strict';

var mongoose = require('mongoose');
var Member = mongoose.model('Members');

exports.list_all_members = function (req, res) {
    Member.find({}, function (err, member) {
        if (err)
            res.send(err);
        res.json(member);
    });
};


exports.create_a_member = function (req, res) {
    var new_member = new Member(req.body);
    new_member.save(function (err, member) {
        if (err)
            res.send(err);
        res.json(member);
    });
};

exports.delete_a_member = function (req, res) {
    Member.deleteOne({
        _id: req.params.memberId
    }, function (err, member) {
        if (err)
            return res.send(err);
        res.json({message: 'Member successfully deleted!'});
    });
};


