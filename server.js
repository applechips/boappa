var express = require('express');
var app = express();
var port = process.env.PORT || 5000;
var mongoose = require('mongoose');
var Member = require('./api/models/membersModel');
var bodyParser = require('body-parser');
var cors = require('cors');

app.use(cors());

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/boappa', {useMongoClient: true});

mongoose.connection.once('connected', function () {
    console.log("Connected to database")
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var routes = require('./api/routes/membersRoutes');
routes(app);

app.listen(port);

app.use(function (req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});

console.log('RESTful API server started on: ' + port);
